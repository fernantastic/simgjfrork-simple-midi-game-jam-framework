using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OSC.NET;
using System;
using System.Threading;

// OSC support, mainly for MAC version
// based on Unity OSC listener by http://mikeheavers.com/main/code-item/osc_receiver_for_unity
// SIMGJFRORK edition by fernando.ramallo@gmail.com
public class ControllerOSC : MonoBehaviour  {
	
	/// a hack to detect button presses in OSC. From this index upward the control changes will be detected as button presses.
	public int firstButtonIndex = 23;
	
	// port to listen to
	public int port = 8338; //MH faceShiftOSC default port
	
	
	// PRIVATE
	private bool connected = false;
	private OSCReceiver receiver;
	private Thread thread;
	
	private List<OSCMessage> processQueue = new List<OSCMessage>();
	
	public int getPort() {
		return port;
	}
	
	public void Start() {
		try {
			Debug.Log("OSC server connected");
			connected = true;
			receiver = new OSCReceiver(port);
			thread = new Thread(new ThreadStart(listen));
			thread.Start();
		} catch (Exception e) {
			Debug.Log("failed to connect to port "+port);
			Debug.Log(e.Message);
		}
	}
	
	/**
	 * Call update every frame in order to dispatch all messages that have come
	 * in on the listener thread
	 */
	public void Update() {
		//processMessages has to be called on the main thread
		//so we used a shared proccessQueue full of OSC Messages
		lock(processQueue){
			foreach( OSCMessage message in processQueue){
				
				BroadcastMessage("OSCMessageReceived", message, SendMessageOptions.DontRequireReceiver);
			}
			processQueue.Clear();
		}
	}
	
	public void OnApplicationQuit(){
		disconnect();
	}
	
	public void disconnect() {
      	if (receiver!=null){
      		 receiver.Close();
      	}
      	
       	receiver = null;
		connected = false;
	}

	public bool isConnected() { return connected; }

	private void listen() {
		while(connected) {
			try {
				OSCPacket packet = receiver.Receive();
				if (packet!=null) {
					lock(processQueue){
						
						//Debug.Log( "adding  packets " + processQueue.Count );
						if (packet.IsBundle()) {
							ArrayList messages = packet.Values;
							for (int i=0; i<messages.Count; i++) {
								processQueue.Add( (OSCMessage)messages[i] );
							}
						} else{
							processQueue.Add( (OSCMessage)packet );
						}
					}
				} else Console.WriteLine("null packet");
			} catch (Exception e) { 
				Debug.Log( e.Message );
				Console.WriteLine(e.Message); 
			}
		}
	}
	
	public void OSCMessageReceived(OSC.NET.OSCMessage message){	
		string address = message.Address;
		ArrayList args = message.Values;

		//Debug.Log(address);
		/*Debug.Log("-- OSC MESSAGE ---");
		foreach( var item in args){
			
			Debug.Log(item + ", " + item.GetType());
		}*/
		
		
		int index = (int)args[1];
		int val = (int)args[2];
		
		// hackishly differentiate between analog and digital controls
		if (index < firstButtonIndex) {
			ValueListener.control_values[index] = (float)val / 127.0f;
			ValueListener.OnControlChange(index);
		} else {
			ValueListener.button_values[index] = val > 64;
			if (ValueListener.button_values[index])
				ValueListener.OnNoteOn(index);
			else
				ValueListener.OnNoteOff(index);
		}
	}
}
