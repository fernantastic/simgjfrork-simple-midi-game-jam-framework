﻿using UnityEngine;
using System.Collections;

// shows how to detect button states, sets the visibility to the button state
public class TestButton : MonoBehaviour {
	public int buttonIndex = 0;
	
	void Update()
	{
		if (renderer.enabled != ValueListener.GetButtonValue(buttonIndex))
			renderer.enabled = ValueListener.GetButtonValue(buttonIndex);
	}
}
